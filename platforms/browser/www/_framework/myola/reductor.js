
/* My-Ola CMS */

//func - on
function route(handle, pathname, response, postData) {
  //console.log("About to route a request for " + pathname);
  if (typeof handle[pathname] === 'function') {
    handle[pathname](response, postData);
  }
  else if (pathname.substr(0,8) == "/assets/" || pathname.substr(0,6) == "/_img/") {
	//console.log("FILE!");
	//console.log(pathname.slice(1));
	handle["/readurl"](response, postData, pathname);
  }
  else {
    //console.log("No request handler found for " + pathname);
    response.writeHead(404, {"Content-Type": "text/plain"});
    response.write("404 Not found");
    response.end();
  }
}
//func - off

//run - on
exports.route = route;
//run - of