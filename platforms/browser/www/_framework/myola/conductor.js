
/* My-Ola CMS */

//vars - on
var http  = require("http");
var url   = require("url");
//vars - of

//func - on
function start(route, handle) {
	//console.log("CN-LOG-1");
  function onRequest(request, response) {
	
	//обращаемся сюда
	var myurl = request.url;
	//console.log("CN-LOG-2: " + myurl);
	var postData = "";
    var pathname = url.parse(myurl).pathname;
    //console.log("CN-LOG-2: " + pathname);
    request.setEncoding("utf8");
	
	//исключение
	var myurlbad=0;
	var myurlbut=myurl.substr(0,8);
	if(myurlbut != "/assets/"){
		//console.log("CN-LOG-3: PAGE");
		
		//post
		request.addListener("data", function(postDataChunk) {
		  postData += postDataChunk;
		  //console.log("Received POST data chunk '"+postDataChunk + "'.");
		});

		//page
		request.addListener("end", function() {
		  route(handle, pathname, response, postData);
		});
	}else{
		//console.log("CN-LOG-3: FILE");

		//post
		request.addListener("data", function(postDataChunk) {
		  //console.log("Received POST data chunk '"+postDataChunk + "'.");
		});

		//page
		request.addListener("end", function() {
		  route(handle, pathname, response, postData);
		});
		
	}
    


  }

  http.createServer(onRequest).listen(9001);
  //console.log("Server has started.");
}
//func - of

//run - on
exports.start = start;
//run - of