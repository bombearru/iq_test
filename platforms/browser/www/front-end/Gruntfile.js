module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: [
                    'assets/js/libs/jquery.min.js',
                    'assets/js/libs/jquery.mobile-1.4.5.min.js',
                    'assets/js/libs/jquery.lavalamp.min.js'
                    //'assets/js/script.js'
                ],
                dest: 'assets/js/build/production.js',
            }
        },
        watch: {
            options:{livereload: true},
            options:{concat: true}
        },
        express:{
            all:{
                options:{
                    port:8500,
                    hostname:'localhost',
                    bases:['.'],
                    livereload: true,
                    concat: true
                }
            }
        }

    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express');

    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
    grunt.registerTask('default', ['concat']);
    grunt.registerTask('server', ['express','watch', 'concat']);

};
