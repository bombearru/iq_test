
/* My-Ola CMS */

//vars - on
var myola_core       = require("./_framework/myola/myola.js");
var conductor        = require("./_framework/myola/conductor");
var reductor         = require("./_framework/myola/reductor");
var pages            = require("./_framework/myola/pages");
//vars - of

//run - on
var handle = {}
handle["/"]            = pages.start;
handle["/start"]       = pages.start;
handle["/signal"]      = pages.signal;
handle["/priyon"]      = pages.priyon;
handle["/json"]        = pages.json;
handle["/filesystem"]  = pages.filesystem;
handle["/readtpl"]     = pages.readtpl;
handle["/readurl"]     = pages.readurl;
conductor.start(reductor.route, handle);
//run - of