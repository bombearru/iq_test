$(document).ready(function(){
    // inputs
    $( "input" ).on("click", function(){
        $(this).parent().next().addClass("active");
        $(this).parent().next().next().addClass("active");
        $(this).parent().next().next().next().addClass("active");
    });
    // navigation
    $( "#enter" ).click(function( event ) {
        event.preventDefault();
        location.hash = "main";
    });
    $( "#game_complete_click" ).click(function( event ) {
        event.preventDefault();
        location.hash = "game_complete";
    });
    // tabs lavalamp
    if ($().lavaLamp) {
        $('.navanimation').lavaLamp({
            fx : 'swing',
            speed : 400,
            enableFocus: true,
            setOnClick:true,
            activeObj: 'selectedLava'
        });
    }
    $(window).load(function() {
        $('.backLava').css('width', $(".selectedLava").width());
    });
    $('.navanimation li:last-child').click(function() {
        $(".backLava").addClass('backLava2');
    });
    $('.navanimation li:nth-child(2)').click(function() {
        $(".backLava").removeClass('backLava2');
    });
    // Таймер обратного отсчета
    function timer() {
        var obj = document.getElementById('timer_inp');
        obj.innerHTML--;
        if (obj.innerHTML == 0) {

            setTimeout(function() {}, 1000);
        } else {
            setTimeout(timer, 1000);
        }
    }
    setTimeout(timer, 1000);
    function timer2() {
        var obj2 = document.getElementById('timer_inp2');

        obj2.innerHTML--;
        if (obj2.innerHTML == 0) {

            setTimeout(function() {}, 1000);
        } else {
            setTimeout(timer2, 1000);
        }
    }
    setTimeout(timer2, 1000);

    // Game Section
    function game_section() {
        var gameCenter = ($("#vopros_text .game_version").width());
        $("#vopros_text .game_version").css("margin-left", -gameCenter);
        var gameCenter2 = ($("#vopros_text3 .game_version").width());
        $("#vopros_text3 .game_version").css("margin-left", -gameCenter2);
    }
    setTimeout(game_section, 1);

    // Высота Элементов в разделе вапросо
    function game_section_height() {
        var navbar,
            navbar2,
            navbar3,
            question,
            question2,
            question3,
            answer,
            answer2,
            answer3,
            footer,
            footer2,
            footer3,
            question_answer,
            question_answer2,
            question_answer3,
            window_height,
            document_height;
        window_height = $( window ).height();
        navbar = $("#vopros_text .navbar").outerHeight();
        navbar2 = $("#vopros .game_image").outerHeight();
        navbar3 = $("#vopros_text3 .navbar").outerHeight();
        answer = $("#vopros_text .game_answer").outerHeight();
        answer2 = $("#vopros .game_answer").outerHeight();
        answer3 = $("#vopros_text3 .game_answer").outerHeight();
        footer = $("#vopros_text #global_nav").outerHeight();
        footer2 = $("#vopros #global_nav").outerHeight();
        footer3 = $("#vopros_text3 #global_nav").outerHeight();
        question_answer = window_height - (29 + navbar + footer);
        question_answer2 = window_height - (navbar2 + footer2);
        question_answer3 = window_height - (navbar3 + footer3);
        question = question_answer - answer;
        question2 = question_answer2 - answer2;
        question3 = question_answer3 - answer3;
        $("#vopros_text .game_question").css("height", question);
        document_height = $( document ).height();

        if(document_height > 567) {
            $("#vopros .game_question").css("height", question2);
            $("#vopros_text3 .game_question").css("height", question3);
        }
        else {
            $("#vopros .game_question").css("height", "inherit");
            $("#vopros_text3 .game_question").css("height", "inherit");
        }
    }
    setTimeout(game_section_height, 1);
    $( window ).resize(function() {
        function game_section_height() {
            var navbar,
                navbar2,
                navbar3,
                question,
                question2,
                question3,
                answer,
                answer2,
                answer3,
                footer,
                footer2,
                footer3,
                question_answer,
                question_answer2,
                question_answer3,
                window_height,
                document_height;
            window_height = $( window ).height();
            navbar = $("#vopros_text .navbar").outerHeight();
            navbar2 = $("#vopros .game_image").outerHeight();
            navbar3 = $("#vopros_text3 .navbar").outerHeight();
            answer = $("#vopros_text .game_answer").outerHeight();
            answer2 = $("#vopros .game_answer").outerHeight();
            ansincwer3 = $("#vopros_text3 .game_answer").outerHeight();
            footer = $("#vopros_text #global_nav").outerHeight();
            footer2 = $("#vopros #global_nav").outerHeight();
            footer3 = $("#vopros_text3 #global_nav").outerHeight();
            question_answer = window_height - (29 + navbar + footer);
            question_answer2 = window_height - (navbar2 + footer2);
            question_answer3 = window_height - (navbar3 + footer3);
            question = question_answer - answer;
            question2 = question_answer2 - answer2;
            question3 = question_answer3 - answer3;
            $("#vopros_text .game_question").css("height", question);
            document_height = $( document ).height();

            if(document_height > 567) {
                $("#vopros .game_question").css("height", question2);
                $("#vopros_text3 .game_question").css("height", question3);
            }
            else {
                $("#vopros .game_question").css("height", "inherit");
                $("#vopros_text3 .game_question").css("height", "inherit");
            }
        }
        setTimeout(game_section_height, 1);
    });
    /*=================================================
    =            Game Screen Active Footer            =
    =================================================*/

        function gameScreenActiveFooterTimer() {
        var gameScreenActiveFooter = $(".gameScreenActiveFooter").height();
        $(".gameScreenActiveFooter li:nth-child(2) a").css("line-height", gameScreenActiveFooter + "px");
        }
        setTimeout(gameScreenActiveFooterTimer, 100);

    /*=====  End of Game Screen Active Footer  ======*/

    /*===========================================
    =            Subject List Height            =
    ===========================================*/

        function Subject() {
            var subjectFooter = $(".subjectScroll").height()+20+60;
            var document_height = $(window).height()-3;

            $(".subjectList").css("min-height", document_height - subjectFooter + "px");

            var subjectList = $(".subjectList ul li").height() / 3;
            var subjectListElement = $(".subjectList ul li a").height() / 3;
            $(".subjectList ul li a").css('margin-top', subjectList - subjectListElement + 10 + "px");
            $(".subjectList ul li a").css('padding-bottom', subjectList - subjectListElement + 10 + "px");

        }
        setTimeout(Subject, 100);

    /*=====  End of Subject List Height  ======*/

    /*===============================================
    =            Scroll To Bottom Button            =
    ===============================================*/

    $('.subjectScroll a').click(function() {
        $('html, body').animate({
            scrollTop: $(document).height()-$(window).height()
        }, 700);
        return false;
    });

    /*=====  End of Scroll To Bottom Button  ======*/

    /*=============================
    =            Modal            =
    =============================*/

    $(window).load(function(){
        // $('#game_final_result_victory').modal('show');
        // $('#game_final_result_draw_modal').modal('show');
        // $('#game_final_result_draw_loss_modal').modal('show');
        // $('#game_final_capitulyacia').modal('show');
        // $('#game_final_zayavka').modal('show');
    });

    /*=====  End of Modal  ======*/

    // Tabs
    $('ul.friendsTabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.friendsTabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    });
    $('#friendsTabs').lavalamp({
        easing: 'easeOutBack',
        enableFocus: true
    });
});
